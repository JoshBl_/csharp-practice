﻿using System;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            //create an instance object from each class
            Rectangle rect = new Rectangle();
            Triangle cone = new Triangle();

            //call the inherited setter for each derived class to initialise all the inherited variable members
            rect.setValues(4, 5);
            cone.setValues(4, 5);

            //call the added method in each derived class to display their values
            Console.WriteLine("Rectangle Area: " + rect.area());
            Console.WriteLine("\nTriangle Area: " + cone.area());
            Console.ReadKey();
        }
    }

    //new class declared - two variable members and one setter method
    public class Polygon
    {
        //protected = accessible only to members of the same class and members derived from that class
        protected int width, height;

        public void setValues (int width, int height)
        {
            this.width = width;
            this.height = height;
        }
    }

    //class that derives from the base class, inheriting members and adding a method
    public class Rectangle : Polygon
    {
        public int area() { return (width * height); }
    }

    //new class that derives from the base class, inherting members and adding a similar method (in the previous class)
    public class Triangle : Polygon
    {
        public int area() { return ((width * height) / 2); }
    }
}
