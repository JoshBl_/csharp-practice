﻿using System;

namespace Cast
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Cast";

            double num = 10.5;
            int integer = 2;

            Console.WriteLine("Value of num: " + num);
            Console.WriteLine("Value of integer: " + integer);

            //implicity cast result of integer value into a double value (and display)
            num = num + integer;
            Console.WriteLine("Implicit cast: " + num);

            //explicity cast result of integer division into a double value (and display)
            num = (double)7 / integer;
            Console.WriteLine("Explicit cast: " + num);

            //cast an integer value into a char data type and display ASCII equivalent
            char letter = (char)65;
            Console.WriteLine("Cast Integer: " + letter);

            //cast a character value into a int data type and display ASCII equivalent
            int ascii = (int)'A';
            Console.WriteLine("Cast Letter: " + ascii);
        }
    }
}
