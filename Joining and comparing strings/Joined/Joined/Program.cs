﻿using System;

namespace Joined
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Joined";

            //new string array called 'a' initialised
            string[] a = new string[3] { "Alpha", "Bravo", "Charlie" };

            //assign a concatenated (joined) version of the first two array elements to a string and display
            string s = string.Concat(a[0], a[1]);
            Console.WriteLine("Concatenated:\t" + s);

            //assign a joined version of the first two array elements with a space separator to a string, then display
            //you specify the char separator first, then the values
            s = String.Join(" ", a[0], a[1]);
            Console.WriteLine("Joined:\t\t" + s);

            //joined version of all elements in the array and tag a separator to the string varaible, then display
            s = String.Join("<br>", a);
            Console.WriteLine("\nHTML:\t" + s + "\n");

            //statements to compare all array elements, alphabetical order
            int num = String.Compare(a[0], a[1]);
            Console.WriteLine(a[0] + " v " + a[1] + ":\t" + num);

            num = String.Compare(a[2], a[1]);
            Console.WriteLine(a[2] + " v " + a[1] + ":\t" + num);

            num = a[1].CompareTo(a[1]);
            Console.WriteLine(a[1] + " v " + a[1] + ":\t" + num + "\n");

            //test the array element values for equality
            bool flag = (a[0] == a[1]);
            Console.WriteLine(a[0] + " == " + a[1] + ":\t\t" + flag);
            flag = a[2].Equals(a[2]);
            Console.WriteLine("a[2]" + " == " + a[2] + ":\t" + flag);
            Console.ReadKey();

        }
    }
}
