﻿using System;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Arrays";

            //an array is not initialised in memory until you make an instance of it - hence the 'new' keyword!
            //this array has 3 elements
            string[] cars = new string[3] { "BMW", "Ford", "Opel" };

            //2 dimensional array
            //each has 3 elements each
            int[,] coords = new int[2, 3] { { 1, 2, 3 }, { 4, 5, 6 } };

            //remember - all arrays start with 0!
            Console.WriteLine("Second Car: " + cars[1]);
            Console.WriteLine("X1,Y1: " + coords[0, 0]);
            Console.WriteLine("X2, Y3: " + coords[1, 2]);
            Console.ReadKey();
        }
    }
}
