﻿using System;
using System.IO;

namespace WriteStream
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "WriteStream";

            //initialise three variables
            string path = "D:\\Repos\\csharp-practice\\Streaming Lines\\joshua.txt";
            string [] poem = new string []
            {
                "\tThis truth finds honest Tam o' Shanter",
                "\tAs he from Ayr one night did canter",
                "\tOld Ayr, which never a town surpasses",
                "\tFor honest men and bonny lasses"
            };
            string attrib = "\r\n\tTam O'Shanter (Robert Burns 1790)";

            try
            {
                //attempt to write the contents of the variable array into a text file
                using (StreamWriter writer = new StreamWriter(path))
                {
                    foreach(string line in poem)
                    {
                        writer.WriteLine(line);
                    }
                }
                //statements that attempt to append the contest of the regular variable into a text file
                //if the file does not exist in this case, it will create it and the parameter has no effect
                //true = append file
                //false = overwrite file
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(attrib);
                    Console.WriteLine("File Written: " + path);
                }
            }
            catch (Exception error)
            {
                //if the file cannot be written
                Console.WriteLine(error.Message);
            }
            Console.ReadKey();
        }
    }
}
