﻿using System;

namespace Variables
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Variables";

            char letter; letter = 'A'; //declared then initialised
            int number; number = 100; //declared then initialised
            float body = 98.6f; //declared and initialised
            double pi = 3.14159; //declared and initialised
            decimal sum = 1000.00m; //declared and initialised
            bool flag = false; //decalred and initialised
            string text = "C# is fun"; //decalred and initialised

            Console.WriteLine("char letter:\t" + letter);
            Console.WriteLine("int number:\t" + number);
            Console.WriteLine("float body:\t" + body);
            Console.WriteLine("double pi:\t" + pi);
            Console.WriteLine("decimal sum:\t" + sum);
            Console.WriteLine("bool flag:\t" + flag);
            Console.WriteLine("string text:\t" + text);
            Console.ReadKey();
        }
    }
}
