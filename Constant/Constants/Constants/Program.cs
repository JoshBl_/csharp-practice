﻿using System;

namespace Constants
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Constant";

            const double pi = 3.14159265358979;
            //var = variant - specify its data type according to the data type of its initial value
            var daysType = typeof (Days);

            //Reveal the constant's data type
            Console.WriteLine("Pi type: " + pi.GetType());
            Console.WriteLine("Circumference: " + (pi * 3));

            //display the name and index position of the first item in the enumerator list
            Console.WriteLine("\nFirst Name: " + Days.Sat);
            Console.WriteLine("1st index: " + (int)Days.Sat);

            //display the name at the second index position and to query the enumerator list
            string name = Enum.GetName(daysType, 1);
            Console.WriteLine("\n2nd index: " + name);
            bool flag = Enum.IsDefined(daysType, "Mon");
            Console.WriteLine("Contains Mon?: " + flag);
            Console.ReadKey();
        }

        //enum = multiple constant values
        //constant (or const) - container to safegaurd a value from change, meaning it can never be changed!
        enum Days {Sat, Sun, Mon, Tue, Wed, Thu, Fri};


    }
}
