﻿using System;

namespace Refactor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Refactor";
            //pass a range of arguments to the methods
            computeFactorials(1, 8);
            Console.ReadKey();

        }

        //recursive method to return factorial value of an integer
        //BEFORE REFACTOR
        //static int factorial (int num)
        //{
        //    int result;
        //    if (num == 1)
        //    {
        //        result = 1;
        //    }
        //    else
        //    {
        //        result = (factorial(num - 1) * num);
        //    }
        //    return result;
        //}

        //AFTER REFACTOR
        //Using ternary operator
        static int factorial(int num)
        {
            return (num == 1) ? 1 : (factorial(num - 1) * num);
        }


        //method to display a range of integers and their computed factorial values

        static void computeFactorials (int num, int max)
        {
            while (num <= max)
            {
                Console.Write("Factorial Of " + num + " : ");
                Console.WriteLine(factorial(num));
                num++;
            }
        }

    }
}
