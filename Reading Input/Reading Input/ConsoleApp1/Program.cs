﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Input";

            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();

            Console.WriteLine("Welcome " + name + "!");
            Console.ReadKey();

            Console.WriteLine("\nPlease enter a number");
            double num = Convert.ToDouble(Console.ReadLine());
            Console.Write("Now enter another number\n");
            double sum = num + Convert.ToDouble(Console.ReadLine());

            Console.WriteLine(name + ", the total is - " + sum);
            Console.ReadKey();
        }
    }
}
