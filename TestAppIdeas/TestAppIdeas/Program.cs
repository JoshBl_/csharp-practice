﻿using System;

namespace TestAppIdeas
{
    class Program
    {
        static void Main(string[] args)
        {
            //bool - this will only be true or false!
            bool check = false;
            //this will always loop whilst the boolean variable, check, is false!
            while (check == false)
            {
                //idea 1 - introducing writing to console, maths and reading input with an if statement
                //while statement as well to illustrate loops
                Console.WriteLine("Hello, what is your full name?");
                string name = Console.ReadLine();
                //string concatenation
                Console.WriteLine("Ah - your name is " + name + "? How old are you?");
                //converting to a different variable type
                string ageString = Console.ReadLine();
                double ageDouble = Convert.ToDouble(ageString);
                Console.WriteLine("You are " + ageDouble + " years old.");
                //using built in methods on variables
                Console.WriteLine("By the way, the length of your name is " + name.Length + " characters");
                double total = name.Length + ageDouble;
                Console.WriteLine("And if we added the length of your name and your age together the total would be... " + total);
                //if statement which is based on the length of the name.
                if (name.Length <= 5)
                {
                    Console.WriteLine("That's a short name!");
                }
                else
                {
                    Console.WriteLine("That's a long name!");
                }
                bool answerCheck = false;
                while (answerCheck == false)
                {
                    Console.WriteLine("Do you want to quit?");
                    string choice = Console.ReadLine();
                    //this will change the input from the user to be uppercase!
                    choice = choice.ToUpper();
                    //printing the input again to the user
                    Console.WriteLine("You said.... " + choice);
                    if (choice == "YES" || choice == "NO")
                    {
                        if (choice == "YES")
                        {
                            Console.WriteLine("Bye!");
                            answerCheck = true;
                            check = true;
                        }
                        else
                        {
                            Console.WriteLine("Back to the start of the loop!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("I don't understand that! Try again!");
                    }
                }
            }
        }
    }
}
