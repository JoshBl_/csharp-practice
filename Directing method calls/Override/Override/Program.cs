﻿using System;

namespace Override
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Override";

            //create new instance of each non-base class
            Pigeon joey = new Pigeon();
            Chicken lola = new Chicken();

            //statement to call methods
            describe(joey);
            describe(lola);
            Console.ReadKey();
        }

        //method to call both overriding methods
        static void describe(Bird obj)
        {
            obj.talk();
            obj.fly();
        }
    }

    //declare a base class, containing two methods that allow overriding (use of the virtual keyword)
    public class Bird
    {
        public virtual void talk()
        {
            Console.WriteLine("A Bird Talks...");
        }

        public virtual void fly()
        {
            Console.WriteLine("A Bird Flies...\n");
        }
    }

    //derived class containing two methods that will override the base class, Bird - by using the override keyword
    public class Pigeon : Bird
    {
        public override void talk()
        {
            Console.WriteLine("Pigeon Says: Coo! Coo!");
        }

        public override void fly()
        {
            Console.WriteLine("A Pigeon Flies Away...");
            //this also calls the base class fly (in Bird) directly!
            base.fly();
        }
    }

    //another derived class containing two methods that will also override the base class methods
    public class Chicken : Bird
    {
        public override void talk()
        {
            Console.WriteLine("Chicken Says: Cluck! Cluck!");
        }

        public override void fly()
        {
            Console.WriteLine("I'm A Chicken - I Can't Fly!");
            base.fly();
        }
    }
}
