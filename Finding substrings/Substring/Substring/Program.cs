﻿using System;

namespace Substring
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Substring";

            string text = "My dog is a cute dog";
            Console.WriteLine(text + "\tLength: " + text.Length);

            //request user for input for another string with a substring to seek
            Console.WriteLine("\nPlease enter a substring to seek: ");
            string sub = Console.ReadLine();

            //character array defined and copy the entire substring value into the array
            char[] arr = new char[sub.Length];
            sub.CopyTo(0, arr, 0, sub.Length);

            //seek first occurence of the substring and call the method defined to report the search results
            int pos = text.IndexOf(sub);
            report(pos, sub);

            //seek the last occurence of the substring and report
            pos = text.LastIndexOf(sub);
            report(pos, sub);

            //seek first occurance of any character of the substring and report
            pos = text.IndexOfAny(arr);
            report(pos, text.Substring(pos, 1));

            //seek last occurence of any character of the substring and report
            pos = text.LastIndexOfAny(arr);
            report(pos, text.Substring(pos, 1));
            Console.ReadKey();
        }

        //method to report the result of a substring search
        static void report(int pos, string sub)
        {
            if(pos != -1)
            { Console.WriteLine("'" + sub + "' Found at " + pos); }
            else
            { Console.WriteLine("'" + sub + "' Not Found!"); }
        }
    }
}
