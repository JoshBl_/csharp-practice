﻿using System;

namespace WhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "WhileLoop";

            //initialise integer array and a regular integer variable
            //single dimension array
            int[] nums = new int[10];
            int i = 0;

            //while loop to assign its incrementing counter value to an array element and display it on each iteration
            while (i < nums.Length)
            {
                nums[i] = i;
                Console.Write(" | " + nums[i]);
                i++;
            }
            Console.Write("\n\n");

            //do-while loop to display its decrementing counter value on each iteration
            do
            {
                i--;
                //when i = 8 0 it prints 'Skipped' and continues
                if (i == 8) { Console.Write(" | Skipped"); continue; }
                //when i = 3 it prints 'Done' and stops
                if (i == 3) { Console.Write(" | Done"); break; }
                Console.Write(" | " + nums[i]);
            }
            while (i > 0);
            Console.ReadKey();
        }
    }
}
