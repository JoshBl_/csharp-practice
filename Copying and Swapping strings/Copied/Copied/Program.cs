﻿using System;

namespace Copied
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Copied";

            //two string variables
            string car1 = "Ghibli";
            string car2 = "GranTurismo";

            //display original values
            Console.WriteLine("Original: \tCar 1: " + car1 + " \t\tCar 2: " + car2);

            //copy the value of the second string variable into the first string and display
            car1 = String.Copy(car2);
            Console.WriteLine("Copied:\t\tCar 1: " + car1 + "\tCar 2: " + car2);

            //integer variable with the length of the first string
            int num = car1.Length;
            //char array of the length
            char[] model = new char[num];

            //copy first string into the character array and display a space separated list of the contents
            car1.CopyTo(0, model, 0, num);
            Console.WriteLine("\nCharacter Array:");
            foreach (char c in model) { Console.Write(c + " ");}

            //remove the end of string (at the start and end) - then display
            car1 = car1.Remove(4);
            Console.WriteLine("\n\nRemoved... \tCar 1: " + car1);

            //insert two strings into the first string, start and end, then display
            car1 = car1.Insert(0, "Maserati ");
            car1 = car1.Insert(13, "Cabrio");
            Console.WriteLine("\nInserted... \tCar 1: " + car1);

            //replace a substring within the first string, then display
            car1 = car1.Replace("GranCabrio", "Quattroporte");
            Console.WriteLine("\nReplaced... \tCar 1: " + car1);

            Console.ReadKey();
        }
    }
}
