﻿using System;

namespace Arithmetic
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Arithmetic";

            int a = 8;
            int b = 4;

            Console.WriteLine("Value of a\t" + a);
            Console.WriteLine("Value of b\t" + b);

            Console.WriteLine("Addition:\t" + (a + b));
            Console.WriteLine("Subtraction:\t" + (a - b));
            Console.WriteLine("Multiplication:\t" + (a * b));
            Console.WriteLine("Division:\t" + (a / b));
            Console.WriteLine("Modulus:\t" + (a % b));

            //postfix increment operation
            Console.WriteLine("\nPostfix:\t" + (a++));
            Console.WriteLine("Postfix result:\t" + a);

            //prefix increment operation
            Console.WriteLine("\nPrefix:\t" + (++b));
            Console.WriteLine("Prefix result:\t" + b);
            Console.ReadKey();
        }
    }
}
