﻿using System;

namespace Condition
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Console";

            //Ternary operator
            //define variables
            int a = 8;
            int b = 3;

            //output appropriate string with correct grammar for quantity
            //as 'a' is not equal to 1, the string 'are' is assigned to verb variable
            string verb = (a != 1) ? " are " : " is ";
            Console.Write("There" + verb + a + "\n");

            //output an appropriate string correctly describing the parity of each variable value
            //a returns 0, so the false statement is triggered
            string parity = (a % 2 != 0) ? "Odd" : "Even";
            Console.Write(a + " is " + parity);

            parity = (b % 2 != 0) ? "Odd" : "Even";
            Console.Write("\n" + b + " is " + parity);

            //output a string reporting the greater of these two variable values
            int max = (a > b) ? a : b;
            Console.Write("\nMaximum is " + max);
            Console.ReadKey();
        }
    }
}
