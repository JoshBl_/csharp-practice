﻿using System;

namespace Features
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Features";

            //request user input to initialise a string variable
            Console.Write("Please Enter Text:");
            string text = Console.ReadLine();

            //conditional test to ensure that user entered input before pressing enter
            if (string.IsNullOrWhiteSpace(text))
            {
                Console.WriteLine("\nERROR: No Text Found!");
            }
            else
            {
                Console.WriteLine("\nThanks. You Entered:\n'" + text + "'");
                Console.WriteLine("\nText Length: " + text.Length);

                string query = text.StartsWith("C#") ? "Does" : "Does Not";
                Console.WriteLine("Text " + query + " Start With 'C#'");

                query = text.EndsWith("steps") ? "Does" : "Does Not";
                Console.WriteLine("Text " + query + " Contain Easy");
            }
            Console.ReadKey();
        }
    }
}
