﻿using System;

namespace Precedence
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Precedence";

            //declaring an integer variable
            int sum;

            //ungrouped expression with the result, in this case, 13
            sum = 1 + 4 * 3;
            Console.Write("Default Order:\t\t" + sum);

            //grouped expression with the result, in this case, 15
            sum = (1 + 4) * 3;
            Console.Write("\nForced Order:\t\t" + sum);

            //assign the result of a new ungrouped expression with the result, in this case 5
            sum = 7 - 4 + 2;
            Console.Write("\nDefault Direction:\t" + sum);

            //assign the result of thr new grouped expression with the result
            sum = 7 - (4 + 2);
            Console.Write("\nForced Direction:\t" + sum);

            Console.ReadKey();
        }
    }
}
