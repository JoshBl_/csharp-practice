﻿using System;

namespace Overload
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Overload";
            //declare two floating point variables
            double num;
            double area;

            //initialise first variable from user input
            Console.Write("Please enter dimension in Feet: ");
            num = Convert.ToDouble(Console.ReadLine());

            //initialise second variable by calling the method that accepts one argument and display value
            area = computeArea(num);
            Console.WriteLine("\nCircle:\t\tArea = " + area + " sq.ft.");

            //Assign a new value to the second varaible by calling the method that accepts two arguments and display value
            area = computeArea(num, num);
            Console.WriteLine("Square:\t\tArea = " + area + " sq.ft.");

            //assign another value to the second variable by calling the method that accepts three arguments and display value
            area = computeArea(num, num, 'T');
            Console.WriteLine("Triangle:\tArea = " + area + " sq.ft.");
            Console.ReadKey();
        }

        //method to return a calculated area of a circle
        static double computeArea(double width)
        {
            double radius = width / 2;
            return ((radius * radius) * 3.141593);
        }

        //method to return a calculated area of a square
        static double computeArea(double width, double height)
        {
            return (width * height);
        }

        //method to reutn the calculated area of a triangle
        static double computeArea(double width, double height, char letter)
        {
            return ((width / 2) * height);
        }

    }
}
