﻿using System;

namespace Hide
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Hide";

            //create an instance of the base class
            Man henry = new Man();

            //create instance of the derived class
            Hombre enrique = new Hombre();

            //call method inheritied by the instance from the base class
            henry.speak();

            //call to the overloaded method from the base class
            henry.speak("It's a beautiful evening");

            //call the hiding method 
            enrique.speak("Hola...");

            //explicit cast to call the overloaded method in the base class
            ((Man)enrique).speak("Es una tarde hermosa");
            Console.ReadKey();
        }
    }

    //new class
    public class Man
    {
        //method with no parameters
        public void speak()
        {
            Console.Write("Hello: ");
        }

        //overloaded method
        public void speak(string message)
        {
            Console.WriteLine(message + "!\n");
        }
    }

    //derived class containing method to intentionally hide the overloaded method of the same name and parameters in the base class
    public class Hombre : Man
    {
        public new void speak(string message)
        {
            //statement to explicity call the simple method in the base class
            base.speak();
            Console.WriteLine(message);
        }
    }
}
