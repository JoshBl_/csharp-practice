﻿using System;

namespace Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Objects";

            //create an instance of Dog class
            Dog fido = new Dog();

            //call new instance objects setter method to initialise all variables
            fido.setValues("Fido", 3, "Brown");

            //retrieve all properties of the new object
            string tagf = String.Format("{0} is a {1} year old {2} dog",
                fido.getName(),
                fido.getAge(),
                fido.getColor());

            //display all properties
            Console.WriteLine(tagf + fido.bark());

            //create another instance of the Dog class
            Dog lucy = new Dog();
            lucy.setValues("Lucy", 2, "Gray");

            //retrieve all properties of the new object
            string tagl = String.Format("{0} is a {1} year old {2} dog",
                lucy.getName(),
                lucy.getAge(),
                lucy.getColor());

            //display all properties of the new object
            Console.WriteLine(tagl + lucy.bark());

            Console.ReadKey();
        }
    }

    //new public class with three variables defined
    public class Dog
    {
        //private so they are only accessible to other members of the same class
        private string name, color;
        private int age;

        //setter method for all variables
        public void setValues (string name, int age, string color)
        {
            //this keyword used to qualify the members of the class (also refers to the current instance of the class)
            this.name = name;
            this.age = age;
            this.color = color;
        }

        //getter methods for each variable
        public string getName() { return name; }
        public int getAge() { return age; }
        public string getColor() { return color; }

        //extra method
        public string bark() { return "\nWoof, woof!\n"; }
    }
}
