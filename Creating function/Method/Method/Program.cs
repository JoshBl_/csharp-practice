﻿using System;

namespace Method
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Method";
            //Add statements to the Main to call each of the methods
            bodyTempC();
            Console.WriteLine("Fahrenheit:\t " + bodyTempF() + "F");
            Console.WriteLine("Kelvin:\t\t " + bodyTempK() + "K");
            Console.ReadKey();

        }

        //static = not intended for use by other classes
        //void = will not return a value
        static void bodyTempC()
        {
            Console.WriteLine("Body Temperature...");
            Console.WriteLine("Centigrade:\t37C");
        }

        //double is defined as it will return a double value
        static double bodyTempF()
        {
            double temperature = 98.6;
            return temperature;
        }

        static int bodyTempK()
        {
            //the variable temperature can be used twice as it only have local scope to the method
            int temperature = 310;
            return temperature;
        }
    }
}
