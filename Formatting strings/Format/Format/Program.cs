﻿using System;

namespace Format
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Format";

            //numeric variable defined, followed by the ToString method to out value as a currency
            int sum = 2500;
            Console.WriteLine("Currency string: " + sum.ToString("C"));

            //String.Fromat used to output same numeric value in different formats
            Console.Write(String.Format("\nGeneral:\t {0:G}", sum));
            Console.Write(String.Format("\nFixed Point:\t {0:F}", sum));
            Console.Write(String.Format("\nNumber:\t\t {0:N}", sum));
            Console.Write(String.Format("\nCurrency:\t {0:C}", sum));

            //reduce numeric value, then output it in a percentage format with padded zeroes
            sum /= 1000;
            Console.Write(String.Format("\nPercentage:\t {0:P}", sum));
            Console.Write(String.Format("\nZero Padded:\t {0:00.0000} \n", sum));

            //comma-separated string list and split it into individual elements of an array
            string data = "Mike,McGrath,Author";
            string[] items = data.Split(',');
            foreach(string item in items)
            {
                Console.Write(String.Format("\n* {0}", item));
            }
            Console.ReadKey();
        }
    }
}
