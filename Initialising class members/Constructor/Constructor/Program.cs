﻿using System;

namespace Constructor
{
    class Program
    {
        static void Main(string[] args)
        {
            //create an instance of class Cat
            Cat tigger = new Cat();

            //retrieve default properties of the new object
            string tagT = String.Format("{0} is a {1} year old {2} cat",
                tigger.getName(),
                tigger.getAge(),
                tigger.getColor());

            //display all properties and method
            Console.WriteLine(tagT + tigger.cry());

            //create another instance of Cat
            Cat smokey = new Cat();

            //set values
            smokey.setName("Smokey");
            smokey.setAge(2);
            smokey.setColor("Gray");

            //retrieve adjusted properties of the new object
            string tagS = String.Format("{0} is a {1} year old {2} cat",
                smokey.getName(),
                smokey.getAge(),
                smokey.getColor());

            Console.WriteLine(tagS + smokey.cry());
            Console.ReadKey();
        }
    }

    //new class with three variables
    public class Cat
    {
        //private variables, only used within the class
        private string name, color;
        private int age;

        //class constructor method to set default values for all variables
        public Cat()
        {
            name = "Tigger";
            age = 3;
            color = "Brown";
        }

        //setter methods
        public void setName(string name) { this.name = name; }
        public void setAge(int age) { this.age = age; }
        public void setColor(string color) { this.color = color; }

        //get methods
        public string getName() { return name; }
        public int getAge() { return age; }
        public string getColor() { return color; }

        //extra method
        public string cry() { return "\nMeow meow!\n"; }
    }
}
