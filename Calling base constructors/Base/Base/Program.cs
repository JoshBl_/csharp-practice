﻿using System;

namespace Base
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Base";
            //create instance of the derived classes
            Daughter anna = new Daughter();
            Son brad = new Son();
            Son carl = new Son(100);
            Console.ReadKey();
        }
    }

    //declare a base class containing a default constructor method
    public class Parent
    {
        public Parent ()
        {
            Console.WriteLine("Parent Called");
        }

        //insert overloaded constructor method into the base calss that requires a single integer argument
        public Parent(int num)
        {
            Console.WriteLine("Parent+ Called: " + num);
        }
    }

    //derived class containing a default constructor method
    public class Daughter : Parent
    {
        public Daughter()
        {
            Console.WriteLine("\tDaughter Called\n");
        }
    }

    //derived class containing a default constructor method
    public class Son : Parent
    {
        public Son()
        {
            Console.WriteLine("\tSon Called");
        }

        //overloaded constructor method into the derived class, which requires an integer argument
        public Son(int num) : base (num)
        {
            Console.WriteLine("\tSon+ Called: " + num);
        }
    }
}
