﻿using System;
using System.IO;

namespace ReadStream
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ReadStream";

            //initialise a variable (file path)
            string path = "D:\\Repos\\csharp-practice\\Manipluating input and output\\TopFive.csv";

            //statement to display a message if the file cannot be read successfully
            try
            {
                //attempt to read the contents of the text file into a variable (line by line)
                using (StreamReader reader = new StreamReader(path))
                {
                    string line;
                    //while line is NOT null - run code
                    while ( (line = reader.ReadLine()) != null)
                    {
                        //statements to modify the case of the column headers and amend an artist name
                        if (line.IndexOf("Rank") != -1) line = line.ToUpper();
                        if (line.IndexOf("Sia") != -1) line += "ft.Sean Paul";

                        //statements that display the content of just two columsn, formatted for alignment
                        string[] sub = line.Split(',');
                        line = String.Format("{0,-30}{1,-20}", sub[1], sub[2]);
                        Console.WriteLine(line);
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            Console.ReadKey();
        }
    }
}
