﻿using System;
using System.IO;

namespace AppendText
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "AppendText";

            //setting the path of where the text file is
            string path = "D:\\Repos\\csharp-practice\\Appending to a file\\oscar.txt";

            //creating a new string array
            string[] poem = new string[]
            {
                "\tIn Debtors' Yard the stones are hard",
                "\tAnd the dripping wall is high"
            };
            //new string variables
            string attrib = "\r\n\tThe Ballad of Reading Gaol";
            attrib += "(Oscar Wilde 1898)";

            if(File.Exists(path))
            {
                //if the file exists, append the text. Specifiying the path first then the contents in the arguments
                File.AppendAllText(path, attrib);
                Console.WriteLine("Appended to File: " + path);
            }
            else
            {
                //statements to attempt to write a text file and confirm success, or warn of failure
                try
                {
                    //specify the path first, then the contents
                    File.WriteAllLines(path, poem);
                    Console.WriteLine("File Written: " + path);
                }
                catch (Exception error)
                {
                    Console.WriteLine(error.Message);
                }
            }
            Console.ReadKey();
        }
    }
}
