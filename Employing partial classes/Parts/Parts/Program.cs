﻿using System;

namespace Parts
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Parts";

            //new object of Sailboat
            Sailboat boat = new Sailboat("Laser", "Classic");

            //call describe method
            boat.describe();
            Console.ReadKey();
        }
    }

    public partial class Sailboat
    {
        //new method
        public void describe()
        {
            Console.WriteLine("Sailboat: {0} {1}", make, model);
        }
    }
}
