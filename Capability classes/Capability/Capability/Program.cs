﻿using System;

namespace Capability
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Capability";

            Pigeon joey = new Pigeon();
            Chicken lola = new Chicken();
            Caller call = new Caller();

            call.describe(joey);
            call.describe(lola);
            Console.ReadKey();
        }
    }

    //a base capability class named "Bird", with two methods
    //abstract used used so the the methods can only be implemented in a derived class
    public abstract class Bird
    {
        public abstract void talk();

        public abstract void fly();
    }

    //derived class containing two methods that will override the base class methods
    //sealed used to safeguard to prevent the class being used as a base class
    public sealed class Pigeon : Bird
    {
        public override void talk()
        {
            Console.WriteLine("Pigeon says: Coo! Coo!");
        }

        public override void fly()
        {
            Console.WriteLine("A Pigeon Flies Away...");
        }
    }

    //new class to override the base class methods
    public sealed class Chicken : Bird
    {
        public override void talk()
        {
            Console.WriteLine("Chicken says: Cluck! Cluck!");
        }

        public override void fly()
        {
            Console.WriteLine("I'm a Chicken - I can't Fly");
        }
    }

    //new class containing method that accepts an object argument
    public sealed class Caller
    {
        public void describe (Bird obj)
        {
            obj.talk();
            obj.fly();
        }
    }
}
