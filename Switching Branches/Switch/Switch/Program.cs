﻿using System;

namespace Switch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Switch";

            //declare integer variable
            int num = 3;

            //declare string variable
            string day;

            //initialise the string variable according to the value of the integer variable
            switch(num)
            {
                case 1:
                    day = "Monday";
                    break;
                case 2:
                    day = "Tuesday";
                    break;
                case 3:
                    day = "Wednesday";
                    break;
                case 4:
                    day = "Thursday";
                    break;
                case 5:
                    day = "Friday";
                    break;
                default:
                    day = "Weekend day";
                    break;
            }

            Console.WriteLine("Day " + num + " : " + day);
            Console.ReadKey();
        }
    }
}
