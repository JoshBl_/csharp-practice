﻿using System;

namespace ForEach
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ForEach";

            //new string array with 5 elements defined
            string[] websites = new string[5] { "Google", "YouTube", "Facebook", "Baidu", "Yahoo" };

            Console.WriteLine("Popular Websites");

            //foreach statement to display a counter value and element value on each iteration
            int rank = 1;
            foreach (string site in websites)
            {
                Console.WriteLine("Position :" + rank + "\t" + site);
                rank++;
            }
            Console.ReadKey();
        }
    }
}
