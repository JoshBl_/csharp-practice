﻿using System;
using System.IO;

namespace ReadText
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ReadText";

            //string to hold path to directory
            string path = "D:\\Repos\\csharp-practice\\Reading text and lines\\word.txt";

            //check if file already exists
            if (File.Exists(path))
            {
                //assign the text file contents to a variable and display
                string text = File.ReadAllText(path);
                Console.WriteLine("File Read: " + path + "\n");
                Console.WriteLine(text + "\n");

                //statements to assign the text file contents to an array and display each element with a counter
                string[] lines = File.ReadAllLines(path);
                int num = 1;
                foreach(string line in lines)
                {
                    Console.WriteLine(num + " : " + line);
                    num++;
                }
            }
            else
            {
                Console.WriteLine("File Not Found: " + path);
            }
            Console.ReadKey();
        }
    }
}
