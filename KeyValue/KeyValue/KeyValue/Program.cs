﻿using System;
using System.Collections.Generic;

namespace KeyValue
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "KeyValue";

            //create and initialise a dictionary called 'BookList'
            Dictionary<string, string> BookList = new Dictionary<string, string>();

            BookList.Add("Stuart Yarnold", "Arduino");
            BookList.Add("Nick Vandome", "Windows 10");
            BookList.Add("Mike McGrath", "Java");

            //display a list heading
            Console.WriteLine("Popular Titles");
            //foreach statement to display each key-value pair
            foreach(KeyValuePair <string, string> book in BookList)
            {
                //in this case, the key is the author and the value is the book name
                Console.WriteLine("Key: " + book.Key + "\tValue " + book.Value + " in easy steps");
            }
            Console.ReadKey();
        }
    }
}
