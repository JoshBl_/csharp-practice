﻿using System;

namespace Comparison
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Comparison";

            //declaring integer and character variables to be compared
            int nil = 0, num = 0, max = 1;

            char cap = 'A', low = 'a';

            //statements to output result of comparison
            Console.Write("Equality:");
            Console.Write("\t (0 == 0) : " + (nil == num));
            Console.Write("\n\t\t (A == a) : " + (cap == low));

            //output results of other integer comparisons
            Console.Write("\n\nInequality:");
            Console.Write("\t (0 != 1) : " + (nil != max));

            Console.Write("\n\nGreater:");
            Console.Write("\t (0 > 1) : " + (nil > max));

            Console.Write("\nLess");
            Console.Write("\t\t (0 < 1) : " + (nil < max));

            Console.Write("\n\nGreater/Equal");
            Console.Write("\t (0 >= 0) : " + (nil >= num));

            Console.Write("\nLess or Equal");
            Console.Write("\t (1 <= 0) : " + (max <= nil));
            Console.ReadKey();
        }
    }
}
