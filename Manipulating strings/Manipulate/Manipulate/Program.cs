﻿using System;

namespace Manipulate
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Manipulate";

            //request user input
            Console.Write("Please Enter Text: ");
            string text = Console.ReadLine();

            //display user input and length
            Console.Write("\nThanks, you entered:\n'" + text + "'");
            Console.WriteLine("\t\tText Length: " + text.Length);

            //Remove leading and trailing whitespace
            //then display manipulated version and length
            text = text.Trim();
            Console.Write("\nTrimmed:\t'" + text + "'");
            Console.WriteLine("\t\tText Length: " + text.Length);

            //uppercase version of the string
            string upper = text.ToUpper();
            Console.WriteLine("\nUpperCase:\t'" + upper + "'");

            //lowercase version of the string
            string lower = text.ToLower();
            Console.WriteLine("LowerCase:\t'" + lower + "'");

            //manipulate all strings to add whitespace and padded characters
            upper = upper.PadLeft(40);
            lower = lower.PadRight(40, '#');
            text = text.PadLeft(30, '*').PadRight(40, '*');

            //display all strings to see padded whitespace and padded characters
            Console.WriteLine("\nPadded Left:\t'" + upper + "'");
            Console.WriteLine("Padded Right:\t'" + lower + "'");
            Console.WriteLine("Padded Both:\t'" + text + "'");

            //display trimmed versions
            Console.WriteLine("\nTrimmed Start:\t'" + upper.TrimStart() + "'");
            Console.WriteLine("Trimmed End:\t'" + text.TrimEnd('*') + "'");
            Console.ReadKey();

        }
    }
}
