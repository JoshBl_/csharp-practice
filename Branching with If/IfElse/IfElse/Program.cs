﻿using System;

namespace IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "IfElse";

            //create and initialise two variables from user input
            Console.Write("Please enter a number\n");
            double num = Convert.ToDouble(Console.ReadLine());

            Console.Write("Thanks, now enter a letter\n");
            char letter = Convert.ToChar(Console.ReadLine());

            //statement to output a message if the user's number exceeds a specified value
            if (num >= 6)
            {
                Console.WriteLine("\nNumber Exceeds 5");
                if (letter == 'C')
                {
                    Console.WriteLine("Letter is C");
                }
            }
            //alternative message
            else
            {
                Console.WriteLine("\nNumber is 5 or less.");
            }
            Console.ReadKey();
        }
    }
}
