﻿using System;

namespace DateFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "DateFormat";

            //initialise a DateTime object and display its value
            DateTime now = DateTime.Now;
            Console.Write("Current Date and Time: " + now);

            //display specific components of the DateTime object
            Console.Write("\nDay Name: " + now.DayOfWeek);
            Console.Write("\nDate Only: " + now.ToShortDateString());
            Console.Write("\nTime Only: " + now.ToShortTimeString());

            //modify DateTime object and display the new value
            now = now.AddYears(4);
            Console.Write("\n\nFuture Date: " + now);

            //create a new DateTime object and display
            DateTime dt = new DateTime(2017, 7, 4, 8, 15, 30);
            Console.Write("\n\nSet Date And Time: {0:f}", dt);

            //display specific components of the new DateTime object
            Console.Write("\nDay Name: {0:dddd}", dt);
            Console.Write("\nLong Date: {0:D}", dt);
            Console.Write("\nLong Time: {0:T}", dt);
            Console.ReadKey();
        }
    }
}
