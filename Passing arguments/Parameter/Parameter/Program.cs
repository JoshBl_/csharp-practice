﻿using System;

namespace Parameter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Parameter";

            //declare two variables
            double weight;
            string num;

            //statements to pass arguments to each of the methods declared
            num = getWeight(out weight);
            Console.WriteLine(num + " lb = " + lbToKg(weight) + " kg");
            kgToLb(ref weight);
            Console.WriteLine(num + " kg = " + weight + " lb");
            Console.ReadKey();
        }

        //method to output a double value and to return a string value
        static string getWeight (out double theWeight)
        {
            theWeight = 10;
            return "Ten";
        }

        //method that returns a multiplied value of its parameter argument
        static double lbToKg (double pounds = 5)
        {
            return (pounds * 0.45359237);
        }

        //method that assigns a divided value to its parameter reference argument
        static void kgToLb (ref double weight)
        {
            weight = (weight / 0.45359237);
        }
    }
}
