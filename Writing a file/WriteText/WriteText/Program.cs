﻿using System;
//declaring a new namespace for using sytem input/output
using System.IO;

namespace WriteText
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "WriteText";

            //initialise two variables
            string path = "D:\\Repos\\csharp-practice\\Writing a file\\poem.txt";
            string poem = "\r\n\tI never saw a man who looked";
            poem += "\r\n\tWith such a wistful eye";
            poem += "\r\n\tUpon that little tent of blue";
            poem += "\r\n\tWhich prisoners call the sky";

            //if statement to check if a file already exists of the specified path and filename
            if (File.Exists(path))
            {
                Console.Write("File Already Exists: " + path);
            }
            else
            {
                try
                {
                    File.WriteAllText(path, poem);
                    Console.WriteLine("File Written: " + path);
                }
                catch(Exception error)
                {
                    Console.WriteLine(error.Message);
                }
            }
            Console.ReadKey();
        }
    }
}
