﻿using System;

namespace Input
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Input and conversion";

            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();

            Console.WriteLine("Welcome " + name + "!");

            Console.WriteLine(name + " please enter a number:");
            double num = Convert.ToDouble(Console.ReadLine());

            Console.Write("Now, enter another number: ");
            double sum = num + Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Total = " + sum);
            Console.ReadKey();
        }
    }
}
