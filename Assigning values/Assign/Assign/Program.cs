﻿using System;

namespace Assign
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Assign";

            int a;
            int b;

            //output simple assigned values
            Console.Write("Assign Values: ");
            Console.Write("\t a = " + (a = 8));
            Console.WriteLine("\t b = " + (b = 4));

            //statements to output combined assigned values
            Console.Write("\n\nAdd and Assign: ");
            Console.Write("\t a += b ( 8+= 4)\t a = " + (a += b));

            Console.Write("\n\nSubtract and Assign: ");
            Console.Write("\t a -= b (12 -= 4)\t a = " + (a -= b));

            Console.Write("\n\nMultiply and Assign: ");
            Console.Write("\t a *= b (8 *= 4)\t a = " + (a *= b));

            Console.Write("\n\nDivide and Assign: ");
            Console.Write("\t a/= b (32 /= 4)\t a = " + (a /= b));

            Console.Write("\n\nModulus and Assign:");
            Console.Write("\t a %= b (8 %= 4)\t a = " + (a %= b));

            Console.ReadKey();
        }
    }
}
